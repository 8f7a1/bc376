# DLMS Book PDF

- [Blue Book](https://www.dlms.com/files/Blue_Book_Edition_13-Excerpt.pdf)
- [Green Book](https://www.dlms.com/files/Green_Book_Edition_9-Excerpt.pdf)

# BluetoothGattService

- Scan Filter: b973f2e0-b19e-11e2-9e96-0800200c9a66
- Write Characteristic: e973f2e2-b19e-11e2-9e96-0800200c9a66
- Read Characteristic: d973f2e1-b19e-11e2-9e96-0800200c9a66 
- Descriptor: 00002902-0000-1000-8000-00805f9b34fb

# obis
- val ActivePower1: String = "1.0.1.8.0.255"
- val ActivePower2: String = "1.0.2.8.0.255"
- val Voltage: String = "1.0.12.3.0.255"

# NodeJS
- Download web: https://nodejs.org/en/download/
- Download link: https://nodejs.org/dist/v18.15.0/node-v18.15.0-win-x64.zip

# MongoDB
- Download link: https://fastdl.mongodb.org/windows/mongodb-windows-x86_64-6.0.4-signed.msi

# Retrofit2
- Website URL: https://square.github.io/retrofit/
- Gradle implement
```gradle
implementation 'com.squareup.retrofit2:retrofit:2.9.0'
implementation 'com.google.code.gson:gson:2.9.1'
implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
```
- Generate class and implement
```kotlin
retrofit = Retrofit.Builder().baseUrl("https://yourdomain.com")
    .addConverterFactory(GsonConverterFactory.create())
    .build()

example = retrofit.create(Example::class.java)
```
- Add permission to AndroidManifest.xml
```xml
<uses-permission android:name="android.permission.INTERNET"/>
```
